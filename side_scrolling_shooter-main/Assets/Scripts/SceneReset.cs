using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneReset : MonoBehaviour
{
    void Update()
    {
        // Si se presiona la tecla "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Reiniciar la escena actual
            ResetScene();
        }
    }

    void ResetScene()
    {
        // Obtener el índice de la escena actual
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        // Cargar la escena actual
        SceneManager.LoadScene(currentSceneIndex);
    }
}
