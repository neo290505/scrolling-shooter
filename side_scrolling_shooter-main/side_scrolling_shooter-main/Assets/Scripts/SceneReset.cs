using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneReset : MonoBehaviour
{
    // Referencia al GameObject del jugador
    public GameObject player;

    void Update()
    {
        // Si se presiona la tecla "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Reiniciar la escena actual
            ResetScene();
        }
    }

    void ResetScene()
    {
        // Obtener el índice de la escena actual
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        // Cargar la escena actual
        SceneManager.LoadScene(currentSceneIndex);

        // Reiniciar la posición del jugador
        if(player != null)
        {
            // Por ejemplo, puedes establecer la posición inicial del jugador aquí
            player.transform.position = Vector3.zero;
        }
    }
}
